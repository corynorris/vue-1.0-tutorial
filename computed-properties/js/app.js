new Vue({
    el: '#app',

    data: {
        points: 1001,
        first: 'Cory',
        last: 'Norris'
    },

    computed: {
        skill: function() {
            if (this.points <= 100) {
                return 'Beginner';
            }
            return 'Advanced'
        },
        fullname: function() {
            return this.first + ' ' + this.last;
        }
    }

});
