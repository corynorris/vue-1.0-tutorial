
// Recommended that you use a compound word to avoid conflicts
Vue.component('tasks',
{
    props: ['list'],
    template: '#tasks-template'
})

new Vue({
    el: '#app',
    data: {
        tasks: [
            {
                body: 'Go to the store',
                completed: false
            },
            {
                body: 'Go to the doctor',
                completed: true
            },
            {
                body: 'Go to the bank',
                completed: false
            }
    	]
    }
});
